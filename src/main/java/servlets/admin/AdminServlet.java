package servlets.admin;

import db.Course;
import db.DBManager;
import db.Users;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet("/admin")
public class AdminServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Users currentUser = (Users) request.getSession().getAttribute("current_user");
        ArrayList<Course> allCourses = DBManager.getCourses();
        ArrayList<Users> allUsers = DBManager.getUsers();
        request.setAttribute("allcourses",allCourses);
        request.setAttribute("allusers",allUsers);

        if (currentUser!=null){


            request.getRequestDispatcher("/admin.jsp").forward(request,response);



        }else
        {
            response.sendRedirect("/login");
        }
    }
}
