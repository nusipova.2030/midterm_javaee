package servlets.student;

import db.Course;
import db.DBManager;
import db.Users;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

@WebServlet("/profile")
public class ProfileServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String courseName = request.getParameter("course");
        String courseId = DBManager.getCourseId(courseName);
        Users currentUser = (Users) request.getSession().getAttribute("current_user");
        DBManager.addCourseToStudent(currentUser.getStudent_id(),courseId);
        response.sendRedirect("/profile");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Users currentUser = (Users) request.getSession().getAttribute("current_user");
        ArrayList<Course> allCourses = DBManager.getCourses();
        request.setAttribute("allcourses",allCourses);

        if (currentUser!=null){
            System.out.println("Current"+currentUser.getSecondName());
            ArrayList<Course> coursesOfStudent = DBManager.getCoursesOfStudent(currentUser.getStudent_id());
            for (Course c: coursesOfStudent){
                System.out.println(c.getName());
            }
            request.setAttribute("courses",coursesOfStudent);
            for (Course c: coursesOfStudent){
                System.out.println(c.getName());}

            request.getRequestDispatcher("/profile.jsp").forward(request,response);



        }else
        {
            response.sendRedirect("/login");
        }
    }
}
