package filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter(filterName = "SessionValidationFilter")
public class SessionValidationFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        String path = ((HttpServletRequest) servletRequest).getRequestURI();
        if("/login.jsp".equals(path) || "/login".equals(path) || "/auth".equals(path)) {
            filterChain.doFilter(servletRequest,servletResponse);
            return;
        }

        String current_user = null;
        HttpSession session = ((HttpServletRequest) servletRequest).getSession(false);
        if (session != null) {
            current_user = (String) session.getAttribute("username");
        }

        if(current_user==null){
            ((HttpServletResponse) servletResponse).sendRedirect("login.jsp");
        }else{
            filterChain.doFilter(servletRequest, servletResponse);
        }
    }

    @Override
    public void destroy() {
        Filter.super.destroy();
    }
}
