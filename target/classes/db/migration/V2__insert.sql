INSERT INTO `courses` (`id`, `course_name`) VALUES
(1, 'java ee'),
(2, 'oracle'),
(3, 'java spring'),
(4, 'bda'),
(5, 'csa'),
(6, 'green technology'),
(7, 'machine learning'),
(8, 'oracle'),
(9, 'oracle');

INSERT INTO `users` (`id`, `first_name`, `second_name`, `student_id`, `password`) VALUES
(1, 'Fariza', 'Nusipova', '26230', 'qwerty'),
(2, 'Adam', 'Adam', 'admin', '123');
INSERT INTO `student_courses` (`id`, `student_id`, `course_id`) VALUES
(NULL, '26230', 1),
(NULL, '26230', 3),
(3, '26230', 5);