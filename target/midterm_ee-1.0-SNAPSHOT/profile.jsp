<%@ page import="db.Course" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="db.DBManager" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>


<html>
<head>
    <title>Profile</title>
    <%@include file="shared/head.jsp" %>

</head>
<body>
<%@include file="shared/navbar_first.jsp" %>
<div class="container">

<div class="jumbotron">
        <h1 class="display-4">Student: <%=currentUser.getFirstName() + " " + currentUser.getSecondName()%>
        </h1>
        <p class="lead">ID: <%=currentUser.getStudent_id()%>
        </p>
        <hr class="my-4">
        <p>Your courses:

        <table class="table">
            <thead>

            <tr>
                <th>ID</th>
                <th>Name</th>
            </tr>
            </thead>
            <%
                ArrayList<Course> courses = (ArrayList<Course>) request.getAttribute("courses");
                if (courses != null) {
                    for (Course c : courses) {
                        System.out.println(c.getName());
            %>
            <tbody>
            <tr>
                <td><% out.print(c.getId()); %>
                </td>
                <td><% out.print(c.getName()); %>
                </td>
            </tr>
            </tbody>
            <%
                    }
                }
            %>
        </table>

        </p>

        <p>Take new courses</p>
        <form action="/profile" method="post">
            <div class="form-group mb-1">
                <label>Courses: </label>
                <select name="course" id="">
                    <%
                        ArrayList<Course> allcourses = (ArrayList<Course>) request.getAttribute("allcourses");
                        if (allcourses != null) {
                            for (Course c : allcourses) {

                    %>
                    <option name="course" ><%=c.getName()%></option>
                    <%
                            }
                        }
                    %>
                </select>
            </div>

            <div class="form-group">
                <button class="btn btn-success">Add</button>
            </div>
        </form>
    </div>
</div>
</body>
</html>
