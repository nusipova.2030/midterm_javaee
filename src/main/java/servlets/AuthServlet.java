package servlets;

import db.DBManager;
import db.Users;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/auth")
public class AuthServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


        String student_id = request.getParameter("student_id");
        String password = request.getParameter("password");

        String redirect = "/login?iderror";

        Users user = DBManager.getUser(student_id);

        if (user != null){
            HttpSession oldSession = request.getSession(false);
            if (oldSession != null) {
                oldSession.invalidate();
            }
            redirect = "/login?passworderror";
            if (user.getPassword().equals(password)){
                request.getSession().setAttribute("current_user", user);
                HttpSession newSession = request.getSession(true);

                newSession.setMaxInactiveInterval(30*60);
                newSession.setAttribute("username", user.getFirstName());
                redirect = "cuser.jsp";
            }
        }
        response.sendRedirect(redirect);

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        if (request.isUserInRole("admin")){
            response.getWriter().write("user admin");
            System.out.println(request.getRemoteUser());
            request.getUserPrincipal();

        }
    }
}
