CREATE TABLE courses (
   `id` int(11) AUTO_INCREMENT PRIMARY KEY,
   `course_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


CREATE TABLE users (
     `id` int(11) AUTO_INCREMENT PRIMARY KEY,
     `first_name` varchar(255) NOT NULL,
     `second_name` varchar(255) NOT NULL,
     `student_id` varchar(11) NOT NULL,
     `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `student_courses` (
       `id` int(11) DEFAULT NULL,
       `student_id` varchar(11) NOT NULL,
       `course_id` int(11) NOT NULL,
       FOREIGN KEY (student_id) REFERENCES courses(id),
       FOREIGN KEY (`student_id`) REFERENCES `users` (`student_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

