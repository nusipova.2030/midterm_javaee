<%@ page import="db.Course" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="db.DBManager" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Admin</title>
    <%@include file="shared/head.jsp" %>
</head>
<body>
<%@include file="shared/navbar_first.jsp" %>
<div class="container">

    <div class="jumbotron">
        <h1 class="display-4">Admin: <%=currentUser.getFirstName() + " " + currentUser.getSecondName()%>
                                     <%-- expression scriplet--%>
        </h1>

        <hr class="my-4">
        <p>All students:

        <table class="table">
            <thead>

            <tr>
                <th>ID</th>
                <th>First Name</th>
                <th>Second Name</th>
                <th>Student ID</th>
                <th>Password</th>
            </tr>
            </thead>
            <%
                ArrayList<Users> users = (ArrayList<Users>) request.getAttribute("allusers");
                if (users != null) {
                    for (Users u : users) {
            %>
            <tbody>
            <tr>
                <td><%=u.getId()%>
                </td>
                <td><%=u.getFirstName()%>
                </td>
                <td><%=u.getSecondName()%>
                </td>
                <td><%=u.getStudent_id()%>
                </td>
                <td><%=u.getPassword()%>
                </td>
            </tr>
            </tbody>
            <%
                    }
                }
            %>
        </table>

        </p>
        <a href="addStudent.jsp">Add student</a>
        <p>All courses:

        <table class="table">
            <thead>

            <tr>
                <th>ID</th>
                <th>Course Name</th>

            </tr>
            </thead>
            <%! ArrayList<Course> courses; %>
            <%
                 courses = (ArrayList<Course>) request.getAttribute("allcourses");
                if (courses != null) {
                    for (Course c : courses) {
            %>
            <tbody>
            <tr>
                <td><%=c.getId()%>
                </td>
                <td><%=c.getName()%>
                </td>

            </tr>
            </tbody>
            <%
                    }
                }
            %>
        </table>

        </p>
        <a href="addCourses.jsp">Add course</a>

    </div>
</div>
</body>
</html>
