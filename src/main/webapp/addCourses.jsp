
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Admin</title>
    <%@include file="shared/head.jsp" %>
</head>
<body>

<%@include file="shared/navbar_first.jsp" %>
<div class="container">
    <form action="/addcourse" method="post">
        <div class="form-group mb-1">
            <label>Course name: </label>
            <input type="text" required name="course_name" class="form-control" />
        </div>
        <div class="form-group">
            <button class="btn btn-success">Add</button>
            <%--                    <input type="submit" value="Login!">--%>
        </div>
    </form>
</div>
</body>
</html>
